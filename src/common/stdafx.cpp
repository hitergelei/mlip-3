/*   MLIP is a software for Machine Learning Interatomic Potentials
 *   MLIP is released under the "New BSD License", see the LICENSE file.
 *   Contributors: Alexander Shapeev, Evgeny Podryabinkin
 */

#include "stdafx.h"

// Note: reference any additional headers you need in STDAFX.H
// and not in this file
